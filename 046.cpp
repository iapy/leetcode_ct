#include <iostream>

#include "include/arrays.hpp"

// https://leetcode.com/problems/permutations/

template<class Array, size_t K, class Enabler = void> struct permutations_helper;
template<class Array, size_t K, size_t I, class Enabler = void> struct iterate_helper;

template<class T, T ... values, size_t K> 
struct permutations_helper<IS<T, values...>, K, std::enable_if_t<K == sizeof ...(values)>> {
    using type = array<IS<T, values...>>;
};

template<class T, T ... values, size_t K> 
struct permutations_helper<IS<T, values...>, K, std::enable_if_t<K != sizeof ...(values)>> {
    using type = typename iterate_helper<IS<T, values...>, K, K>::type;
};

template<class T, T ... values, size_t K, size_t I> 
struct iterate_helper<IS<T, values...>, K, I, std::enable_if_t<I < sizeof ...(values)>> {
    using type = join_arrays
    <
        typename permutations_helper<pop_element<IS<T, values...>, K, I>, K + 1>::type,
        typename iterate_helper<IS<T, values...>, K, I + 1>::type
    >;
};

template<class T, T ... values, size_t K, size_t I> 
struct iterate_helper<IS<T, values...>, K, I, std::enable_if_t<I == sizeof ...(values)>> {
    using type = array<>;
};

template<class T>
using permutations = typename permutations_helper<T, 0>::type;

int main()
{
    each(permutations<AI<1,2,3,4>>{}, [](auto p){
        each(p, [](auto q){
            std::cout << q << ' ';
        });
        std::cout << '\n';
    });
    return 0;
}