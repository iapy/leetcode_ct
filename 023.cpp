#include <iostream>

#include "include/arrays.hpp"

// https://leetcode.com/problems/merge-k-sorted-lists/

int main()
{
    using result = merged<
        sorted<AI<5, 3, 7, 4, 1, 6>>,
        sorted<AI<5, 3, 2, 4, 1, 6>>,
        sorted<AI<1, 9, 3>>,
        sorted<AI<8, 4, 7>>
    >;
    each(result{}, [](auto p){
        std::cout << p << ' ';
    });
    std::cout << '\n';
    return 0;
}