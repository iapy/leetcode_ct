%: %.cpp
	/usr/bin/clang++ --std=c++17 -o $@.tsk -I/Users/mika/Projects/c++ $<

clean:
	/usr/bin/find . -name "*.cpp" | /usr/bin/cut -f2 -d/ | /usr/bin/cut -f1 -d. | xargs -I'{}' echo {}.tsk | xargs rm -rf
	/usr/bin/find . -name "*.cpp" | /usr/bin/cut -f2 -d/ | /usr/bin/cut -f1 -d. | xargs -I'{}' echo {}.dSYM | xargs rm -rf

.PHONY: clean