#include <iostream>
#include "include/arrays.hpp"
#include "include/integers.hpp"

// https://leetcode.com/problems/palindrome-number/

template<int value>
constexpr bool palindrome = (
    (value > 0) && std::is_same_v<
        typename to_decimal_digits<value>::digits,
        reverse_array<typename to_decimal_digits<value>::digits>
    >
);

int main()
{
    std::cout << std::boolalpha << palindrome< 121> << '\n';
    std::cout << std::boolalpha << palindrome<-121> << '\n';
    std::cout << std::boolalpha << palindrome<8193> << '\n';
    std::cout << std::boolalpha << palindrome<1221> << '\n';    
    return 0;
}