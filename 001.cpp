#include <iostream>
#include "include/arrays.hpp"

// https://leetcode.com/problems/two-sum/

template<class T, typename T::value_type, size_t N> struct two_sum_helper;

template<class T, T head, T ... tail, T value, std::size_t N>
struct two_sum_helper<IS<T, head, tail...>, value, N> : public std::conditional<
    index_of<IS<T, head, tail...>, value - head> == std::numeric_limits<std::size_t>::max(),
    typename two_sum_helper<IS<T, tail...>, value, N + 1>::type,
    IS<std::size_t, N, N + index_of<IS<T, head, tail...>, value - head>>
> {};

template<class T, T head, T value, std::size_t N>
struct two_sum_helper<IS<T, head>, value, N> {
    using type = IS<size_t,  std::numeric_limits<std::size_t>::max(), std::numeric_limits<std::size_t>::max()>;
};

template<class T, typename T::value_type V>
using two_sum = typename two_sum_helper<T, V, 0>::type;


int main()
{
    using values = AI<2, 7, 11, 15>;
    each(two_sum<values, 9>{}, [](auto v){
        std::cout << v << " ";
    });
    std::cout << "\n";
    each(two_sum<values, 13>{}, [](auto v){
        std::cout << v << " ";
    });
    std::cout << "\n";
    each(two_sum<values, 18>{}, [](auto v){
        std::cout << v << " ";
    });
    std::cout << "\n";
    each(two_sum<values, 17>{}, [](auto v){
        std::cout << v << " ";
    });
    std::cout << "\n";
    return 0;
}