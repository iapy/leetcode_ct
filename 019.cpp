#include <iostream>

#include "include/arrays.hpp"

// https://leetcode.com/problems/remove-nth-node-from-end-of-list/

template<class T, std::size_t N>
using remove_nth_last = reverse_array<remove_from_array<reverse_array<T>, N>>;

int main()
{
    each(remove_nth_last<AI<1,2,3,4,5>, 0>{}, [](auto p) {
        std::cout << p << ' ';
    });
    std::cout << '\n';
    each(remove_nth_last<AI<1,2,3,4,5>, 1>{}, [](auto p) {
        std::cout << p << ' ';
    });
    std::cout << '\n';
    each(remove_nth_last<AI<1,2,3,4,5>, 2>{}, [](auto p) {
        std::cout << p << ' ';
    });
    std::cout << '\n';
    each(remove_nth_last<AI<1,2,3,4,5>, 3>{}, [](auto p) {
        std::cout << p << ' ';
    });
    std::cout << '\n';
    each(remove_nth_last<AI<1,2,3,4,5>, 4>{}, [](auto p) {
        std::cout << p << ' ';
    });
    std::cout << '\n';
    return 0;
}