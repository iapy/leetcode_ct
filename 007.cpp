#include <iostream>
#include "include/arrays.hpp"
#include "include/integers.hpp"

// https://leetcode.com/problems/reverse-integer/

template<int value>
struct reverse_integer
{
    constexpr static bool negative = (value < 0);
    using digits = reverse_array<typename to_decimal_digits<value>::digits>;
};

template<int value>
constexpr int reversed = from_decimal_digits<reverse_integer<value>>;

int main()
{
    std::cout << reversed<8193> << '\n';
    std::cout << reversed<-742> << '\n';
    return 0;
}