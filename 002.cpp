#include <iostream>
#include "include/arrays.hpp"

// https://leetcode.com/problems/add-two-numbers/

template<class Result, class A, class B, unsigned C>
struct add_two_numbers_helper;

template<unsigned ...R, unsigned VH, unsigned ...VT, unsigned UH, unsigned ...UT, unsigned C>
struct add_two_numbers_helper<AU<R...>, AU<VH, VT...>, AU<UH, UT...>, C>
{
    using type =  typename add_two_numbers_helper<
        join_arrays<
            AU<R...>,
            AU<(VH + UH + C) % 10>
        >,
        AU<VT...>,
        AU<UT...>,
        (VH + UH + C) / 10
    >::type;
};

template<unsigned ...R, unsigned UH, unsigned ...UT, unsigned C>
struct add_two_numbers_helper<AU<R...>, AU<>, AU<UH, UT...>, C>
{
    using type = typename add_two_numbers_helper<
        join_arrays<
            AU<R...>,
            AU<(UH + C) % 10>
        >,
        AU<>,
        AU<UT...>,
        (UH + C) / 10
    >::type;
};

template<unsigned ...R, unsigned VH, unsigned ...VT, unsigned C>
struct add_two_numbers_helper<AU<R...>, AU<VH, VT...>, AU<>, C>
{
    using type = typename add_two_numbers_helper<AU<R...>, AU<>, AU<VH, VT...>, C>::type;
};

template<unsigned ...R, unsigned C>
struct add_two_numbers_helper<AU<R...>, AU<>, AU<>, C>
{
    using type = join_arrays<AU<R...>, AU<C>>;
};

template<unsigned ...R>
struct add_two_numbers_helper<AU<R...>, AU<>, AU<>, 0>
{
    using type = AU<R...>;
};

template<class T, class U>
using add_two_numbers = typename add_two_numbers_helper<AU<>, T, U, 0>::type;


int main()
{
    each(add_two_numbers<AU<2,4,3>, AU<5,6,4>>{}, [](auto value){
        std::cout << value;
    });
    std::cout << '\n';
    each(add_two_numbers<AU<2,4,5,1>, AU<5,6,4>>{}, [](auto value){
        std::cout << value;
    });
    std::cout << '\n';
    return 0;
}