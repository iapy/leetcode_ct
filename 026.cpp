#include <iostream>

#include "include/arrays.hpp"

// https://leetcode.com/problems/remove-duplicates-from-sorted-array/

template<class T> struct remove_duplicates_helper;

template<class T, T a, T b, T ... tail>
struct remove_duplicates_helper<IS<T, a, b, tail...>>
{
    using type = std::conditional_t
    <
        (a == b),
        typename remove_duplicates_helper
        <
            IS<T, a, tail...>
        >::type,
        join_arrays
        <
            IS<T, a>,
            typename remove_duplicates_helper
            <
                IS<T, b, tail...>
            >::type
        >
    >;
};

template<class T, T a>
struct remove_duplicates_helper<IS<T, a>>
{
    using type = IS<T, a>;
};

template<class T>
struct remove_duplicates_helper<IS<T>>
{
    using type = IS<T>;
};

template<class T>
using remove_duplicates = typename remove_duplicates_helper<T>::type;

int main()
{
    each(remove_duplicates<AI<1, 1, 2, 2, 3, 3, 3, 3>>{}, [](auto p){
        std::cout << p << ' ';
    });
    std::cout << '\n';
    return 0;
}