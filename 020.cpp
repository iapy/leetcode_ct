#include <iostream>

#include "include/arrays.hpp"
#include "include/string.hpp"

// https://leetcode.com/problems/valid-parentheses/

template<class T, class C, class Enabler = void> struct valid_parentheses_helper;

template<char head, char ... tail, char ... stack>
struct valid_parentheses_helper<string<head, tail...>, string<stack...>,
    std::enable_if_t<(head == '{' || head == '(' || head == '[')>>
: public std::integral_constant<bool, valid_parentheses_helper<string<tail...>, string<head, stack...>>::value>
{};

template<char head, char ... tail, char stack_head, char ... stack>
struct valid_parentheses_helper<string<head, tail...>, string<stack_head, stack...>,
    std::enable_if_t<(head == '}' || head == ')' || head == ']')>>
: public std::integral_constant<bool, (false
    || (head == '}' && stack_head == '{') 
    || (head == ')' && stack_head == '(')
    || (head == ']' && stack_head == '[')
) && valid_parentheses_helper<string<tail...>, string<stack...>>::value>
{};

template<char head, char ... tail>
struct valid_parentheses_helper<string<head, tail...>, string<>,
    std::enable_if_t<(head == '}' || head == ')' || head == ']')>>
: public std::integral_constant<bool, false>
{};

template<char ... stack>
struct valid_parentheses_helper<string<>, string<stack...>,
    std::enable_if_t<true>>
: public std::integral_constant<bool, (sizeof ... (stack) == 0)>
{};

template<class T>
constexpr bool valid_parentheses = valid_parentheses_helper<T, string<>>::value;

int main()
{
    std::cout << std::boolalpha << valid_parentheses<string<'(', ')'>> << '\n';
    std::cout << std::boolalpha << valid_parentheses<string<'(', '}'>> << '\n';
    std::cout << std::boolalpha << valid_parentheses<string<')', ']'>> << '\n';
    std::cout << std::boolalpha << valid_parentheses<string<'(', '{', ')', '}'>> << '\n';
    std::cout << std::boolalpha << valid_parentheses<string<'(', '{', '}', '[', ']', ')'>> << '\n';
    return 0;
}