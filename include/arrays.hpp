#ifndef INCLUDE_ARRAYS_HPP
#define INCLUDE_ARRAYS_HPP

#include <utility>

template<class T, T ... values>
using IS = std::integer_sequence<T, values...>;

template<int ... values>
using AI = IS<int, values...>;

template<unsigned ... values>
using AU = IS<unsigned, values...>;

template<class T, T ... values, class F>
void each(IS<T, values...>, F const &f)
{
    int dummy[]{(f(values), 0)...};
}

template<class ... T>
struct array
{
    static constexpr std::size_t size() noexcept
    {
        return sizeof...(T);
    }
};

template<class ... T, class F>
void each(array<T...>, F const &f)
{
    int dummy[]{(f(T{}), 0)...};
}

// {{ searching element in array
template<class T, typename T::value_type, std::size_t N> struct index_of_helper;

template<class T, T head, T ... tail, T value, std::size_t N>
struct index_of_helper<IS<T, head, tail...>, value, N> : public std::integral_constant<
    std::size_t, head == value ? N : index_of_helper<IS<T, tail...>, value, N + 1>::value
>{};

template<class T, T head, T value, std::size_t N>
struct index_of_helper<IS<T, head>, value, N> : public std::integral_constant<
    std::size_t, head == value ? N : std::numeric_limits<std::size_t>::max()
>{};

template<class T, typename T::value_type V> 
static constexpr std::size_t index_of = index_of_helper<T, V, 0>::value;
// }} searching element in array

// {{ join arrays
template<class, class> struct join_arrays_helper;

template<class T, T ... v, T ... u>
struct join_arrays_helper<IS<T, v...>, IS<T, u...>>
{
    using type = IS<T, v..., u...>;
};

template<class ... A, class ... B>
struct join_arrays_helper<array<A...>, array<B...>>
{
    using type = array<A..., B...>;
};

template<class A, class B>
using join_arrays = typename join_arrays_helper<A, B>::type;
// }} join arrays

// {{ reverse array
template<class T> struct reverse_array_helper;

template<class T, T head, T ... tail>
struct reverse_array_helper<IS<T, head, tail...>>
{
    using type = join_arrays<typename reverse_array_helper<IS<T, tail...>>::type, IS<T, head>>;
};

template<class T, T head>
struct reverse_array_helper<IS<T, head>>
{
    using type = IS<T, head>;
};

template<class Head, class ... Tail>
struct reverse_array_helper<array<Head, Tail...>> {
    using type = array<Tail..., Head>;
};

template<class A>
using reverse_array = typename reverse_array_helper<A>::type;
// }} reverse array

// {{ subarrays
template<size_t, size_t, class, class Enable = void> struct subarray_helper;

template<size_t B, size_t S, class T, T head, T... tail> 
struct subarray_helper<B, S, IS<T, head, tail...>, std::enable_if_t<B != 0 && S != 0>>
{
    using type = typename subarray_helper<B - 1, S, IS<T, tail...>>::type;
};

template<size_t B, class T, T... tail> 
struct subarray_helper<B, 0, IS<T, tail...>, std::enable_if_t<B != 0>>
{
    using type = std::enable_if_t<B <= sizeof...(tail), IS<T>>;
};

template<size_t S, class T, T head, T... tail> 
struct subarray_helper<0, S, IS<T, head, tail...>, std::enable_if_t<S != 0>>
{
    using type = join_arrays<IS<T, head>, typename subarray_helper<0, S - 1, IS<T, tail...>>::type>;
};

template<class T, T... tail> 
struct subarray_helper<0, 0, IS<T, tail...>, std::enable_if_t<true>>
{
    using type = IS<T>;
};

template<class T, size_t B, size_t S>
using subarray = typename subarray_helper<B, S, T>::type;

// tests
static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 0, 4>, AI<1,2,3,4>
>);

static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 0, 1>, AI<1>
>);

static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 1, 3>, AI<2,3,4>
>);

static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 2, 2>, AI<3,4>
>);

static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 2, 1>, AI<3>
>);

static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 1, 0>, AI<>
>);

static_assert(std::is_same_v<
    subarray<AI<1,2,3,4>, 3, 0>, AI<>
>);

// }} subarrays

// {{ merge
template<class ... A> struct merged_helper;

template<class T, T head1, T... tail1, T head2, T ... tail2, class ... Tail>
struct merged_helper<IS<T, head1, tail1...>, IS<T, head2, tail2...>, Tail...>
{
    using type = typename merged_helper<std::conditional_t<
        head1 < head2,
        join_arrays<IS<T, head1>, typename merged_helper<IS<T, tail1...>, IS<T, head2, tail2...>>::type>,
        join_arrays<IS<T, head2>, typename merged_helper<IS<T, head1, tail1...>, IS<T, tail2...>>::type>
    >, Tail...>::type;
};

template<class T, T ... tail>
struct merged_helper<IS<T>, IS<T, tail...>>
{
    using type = IS<T, tail...>;
};

template<class T, T ... tail>
struct merged_helper<IS<T, tail...>, IS<T>>
{
    using type = IS<T, tail...>;
};

template<class T>
struct merged_helper<T>
{
    using type = T;
};

template<class ... A>
using merged = typename merged_helper<A...>::type;
// }} merge

// {{ mergesort
template<class A, class Enable = void> struct sorted_helper;

template<class T, T... tail>
struct sorted_helper<IS<T, tail...>, std::enable_if_t<(sizeof ...(tail) > 1)>>
{
private:
    using left  = subarray<IS<T, tail...>, 0, ((sizeof...(tail)) >> 1)>;
    using right = subarray<IS<T, tail...>, ((sizeof...(tail)) >> 1), (sizeof ... (tail)) - ((sizeof...(tail)) >> 1)>;
public:
    using type = merged<
        typename sorted_helper<left>::type,
        typename sorted_helper<right>::type
    >;
};

template<class T, T... tail>
struct sorted_helper<IS<T, tail...>, std::enable_if_t<(sizeof ...(tail) <= 1)>>
{
    using type = IS<T, tail...>;
};

template<class A>
using sorted = typename sorted_helper<A>::type;
// }} mergesort

// {{ accessing elements
template<class T, std::size_t N, class Enable = void> struct get_element_of_array_helper;

template<class T, T head, T ... tail, size_t N>
struct get_element_of_array_helper<IS<T, head, tail...>, N, std::enable_if_t<N != 0>>
: public std::integral_constant<T, get_element_of_array_helper<IS<T, tail...>, N - 1>::value>
{};

template<class T, T head, T ... tail, size_t N>
struct get_element_of_array_helper<IS<T, head, tail...>, N, std::enable_if_t<N == 0>>
: public std::integral_constant<T, head>
{};

template<class T, size_t N>
constexpr typename T::value_type get_element_of_array = get_element_of_array_helper<T, N>::value;

// test
static_assert(get_element_of_array<AI<1,2,3,4>, 0> == 1);
static_assert(get_element_of_array<AI<1,2,3,4>, 1> == 2);
static_assert(get_element_of_array<AI<1,2,3,4>, 2> == 3);
static_assert(get_element_of_array<AI<1,2,3,4>, 3> == 4);
// }} accessing elements

// {{ removing elements
template<class T, std::size_t N, class Enable = void> struct remove_from_array_helper;

template<class T, T head, T ... tail, size_t N>
struct remove_from_array_helper<IS<T, head, tail...>, N, std::enable_if_t<N != 0>>
{
    using type = join_arrays<IS<T, head>, typename remove_from_array_helper<IS<T, tail...>, N - 1>::type>;
};

template<class T, T head, T ... tail, size_t N>
struct remove_from_array_helper<IS<T, head, tail...>, N, std::enable_if_t<N == 0>>
{
    using type = IS<T, tail...>;
};

template<class T, size_t N>
using remove_from_array = typename remove_from_array_helper<T, N>::type;

// tests
static_assert(std::is_same_v<
    remove_from_array<AI<1,2,3,4>, 0>, AI<2,3,4>
>);

static_assert(std::is_same_v<
    remove_from_array<AI<1,2,3,4>, 1>, AI<1,3,4>
>);

static_assert(std::is_same_v<
    remove_from_array<AI<1,2,3,4>, 2>, AI<1,2,4>
>);

static_assert(std::is_same_v<
    remove_from_array<AI<1,2,3,4>, 3>, AI<1,2,3>
>);
// }} removing elements

// {{ swapping elements 
template<class T, std::size_t N, size_t M> struct swap_elements_helper;

template<class T, T ... tail, std::size_t N, size_t M>
struct swap_elements_helper<IS<T, tail...>, N, M>
{
    using type = join_arrays
    <
        join_arrays
        <
            join_arrays
            <
                subarray<IS<T, tail...>, 0, N>,
                IS<T, get_element_of_array<IS<T, tail...>, M>>
            >,
            join_arrays
            <
                subarray<IS<T, tail...>, N + 1, M - N - 1>,
                IS<T, get_element_of_array<IS<T, tail...>, N>>
            >
        >,
        subarray<IS<T, tail...>, M + 1, sizeof ... (tail) - M - 1>
    >;
};

template<class T, T ... tail, std::size_t N>
struct swap_elements_helper<IS<T, tail...>, N, N>
{
    using type = IS<T, tail...>;
};

template<class T, std::size_t N, size_t M>
using swap_elements = typename swap_elements_helper<T, N, M>::type;

// tests
static_assert(std::is_same_v<
    swap_elements<AI<1,2,3,4>, 0, 1>, AI<2,1,3,4>
>);

static_assert(std::is_same_v<
    swap_elements<AI<1,2,3,4>, 0, 2>, AI<3,2,1,4>
>);

static_assert(std::is_same_v<
    swap_elements<AI<1,2,3,4>, 0, 3>, AI<4,2,3,1>
>);

static_assert(std::is_same_v<
    swap_elements<AI<1,2,3,4>, 1, 2>, AI<1,3,2,4>
>);

static_assert(std::is_same_v<
    swap_elements<AI<1,2,3,4>, 1, 1>, AI<1,2,3,4>
>);
// }} swapping elements 

// {{ popping elements
template<class T, std::size_t N, size_t M, class Enabler = void> struct pop_element_helper;

template<class T, T ... tail, std::size_t N, size_t M>
struct pop_element_helper<IS<T, tail...>, N, M, std::enable_if_t<N <= M>>
{
    using type = join_arrays
    <        
        join_arrays
        <
            subarray<IS<T, tail...>, 0, N>,
            IS<T, get_element_of_array<IS<T, tail...>, M>>
        >,
        join_arrays
        <
            subarray<IS<T, tail...>, N, M - N>,
            subarray<IS<T, tail...>, M + 1, sizeof ... (tail) - M - 1>
        >   
    >;
};

template<class T, std::size_t N, std::size_t M>
using pop_element = typename pop_element_helper<T, N, M>::type;

// tests
static_assert(std::is_same_v<
    pop_element<AI<1,2,3,4>, 0, 1>, AI<2,1,3,4>
>);

static_assert(std::is_same_v<
    pop_element<AI<1,2,3,4>, 0, 2>, AI<3,1,2,4>
>);

static_assert(std::is_same_v<
    pop_element<AI<1,2,3,4>, 1, 3>, AI<1,4,2,3>
>);
// }} popping elements
#endif
