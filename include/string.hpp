#ifndef INCLUDE_STRING_HPP
#define INCLUDE_STRING_HPP
#include <iostream>

template<char ... Values>
using string = IS<char, Values...>;

template<char ... Tail>
std::ostream &operator << (std::ostream &stream, string<Tail...> const &v) {
    each(v, [&stream](auto value) { stream << value; });
    return stream;
}

#endif