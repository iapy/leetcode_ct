#ifndef INCLUDE_INTEGERS_HPP
#define INCLUDE_INTEGERS_HPP

template<class, int>
struct integer_to_decimal_digits_helper;

template<unsigned ... R, int value>
struct integer_to_decimal_digits_helper<AU<R...>, value>
{
    constexpr static bool negative = (value < 0);
    using digits = typename integer_to_decimal_digits_helper<
        join_arrays<
            AU<((value > 0 ? value : -value) % 10)>,
            AU<R...>
        >,
        (value / 10)
    >::digits;
};

template<unsigned ... R>
struct integer_to_decimal_digits_helper<AU<R...>, 0>
{
    constexpr static bool negative = false;
    using digits = AU<R...>;
};

template<int value>
using to_decimal_digits = integer_to_decimal_digits_helper<AU<>, value>;

template<class, int> struct decimal_digits_to_integer_helper;

template<unsigned P, unsigned ... R, int result>
struct decimal_digits_to_integer_helper<AU<P, R...>, result>
{
    constexpr static int value = decimal_digits_to_integer_helper<AU<R...>, (10 * result + P)>::value;
};

template<unsigned P, int result>
struct decimal_digits_to_integer_helper<AU<P>, result>
{
    constexpr static int value = (10 * result + P);
};

template<class Digits>
constexpr int from_decimal_digits = Digits::negative
    ? -decimal_digits_to_integer_helper<typename Digits::digits, 0>::value
    :  decimal_digits_to_integer_helper<typename Digits::digits, 0>::value
;

#endif