#include <iostream>

#include "include/arrays.hpp"

// https://leetcode.com/problems/swap-nodes-in-pairs/

template<class T> struct swap_helper;

template<class A, class B, class ... T>
struct swap_helper<array<A, B, T...>>
{
    using type = join_arrays<array<B, A>, typename swap_helper<array<T...>>::type>;
};

template<class A, class B>
struct swap_helper<array<A, B>>
{
    using type = array<B, A>;
};

template<class A>
struct swap_helper<array<A>>
{
    using type = array<A>;
};

template<class T, T A, T B, T ... tail>
struct swap_helper<IS<T, A, B, tail...>>
{
    using type = join_arrays<IS<T, B, A>, typename swap_helper<IS<T, tail...>>::type>;
};

template<class T, T a>
struct swap_helper<IS<T, a>>
{
    using type = IS<T, a>;
};

template<class T>
struct swap_helper<IS<T>>
{
    using type = IS<T>;
};

template<>
struct swap_helper<array<>>
{
    using type = array<>;
};

template<class T>
using swap = typename swap_helper<T>::type;

static_assert(std::is_same_v<
    swap<array<int, float, double, std::string>>,
    array<float, int, std::string, double>
>);

int main()
{
    each(swap<AI<1,2,3,4,5,6,7>>{}, [](auto p){
        std::cout << p << ' ';
    });
    std::cout << '\n';
    each(swap<AI<1,2,3,4,5,6>>{}, [](auto p){
        std::cout << p << ' ';
    });
    std::cout << '\n';
    return 0;
}