#include <iostream>

#include "include/arrays.hpp"

// https://leetcode.com/problems/merge-two-sorted-lists/

int main()
{
    each(merged<sorted<AI<5, 3, 7, 4, 1, 6>>, sorted<AI<5, 3, 2, 4, 1, 6>>>{}, [](auto p){
        std::cout << p << ' ';
    });
    std::cout << '\n';
    return 0;
}