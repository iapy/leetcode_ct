#include <iostream>

#include "include/arrays.hpp"
#include "include/string.hpp"

// https://leetcode.com/problems/generate-parentheses/

template<class S> struct opened_helper;

template<char Head, char ... Tail>
struct opened_helper<string<Head, Tail...>>
{
    constexpr static int64_t value = (Head == '(' ? 1 : 0) + opened_helper<string<Tail...>>::value;
};

template<>
struct opened_helper<string<>>
{
    constexpr static int64_t value = 0;
};

template<class T>
constexpr int64_t opened = opened_helper<T>::value;

template<class Array, size_t N, class Enable = void> struct append_character;

template<class Head, class ... Tail, size_t N>
struct append_character<array<Head, Tail...>, N, std::enable_if_t<(sizeof...(Tail) != 0)>>
{
    using type = join_arrays<
        typename append_character<array<Head>, N>::type,
        typename append_character<array<Tail...>, N>::type
    >;
};

template<size_t N>
struct append_character<array<>, N, void>
{
    using type = typename append_character<array<string<'('>>, N>::type;
};

template<>
struct append_character<array<>, 0, void>
{
    using type = array<>;
};

template<class Head, size_t N>
struct append_character<array<Head>, N,
                        std::enable_if_t<Head::size() == (N << 1)>>
{
    using type = array<Head>;
};

template<class Head, size_t N>
struct append_character<array<Head>, N,
                        std::enable_if_t<Head::size() < (N << 1) && opened<Head> == (Head::size() >> 1)>>
{
    using type = typename append_character<
        array<join_arrays<Head, string<'('>>>, N
    >::type;
};

template<class Head, size_t N>
struct append_character<array<Head>, N,
                        std::enable_if_t<Head::size() < (N << 1) && opened<Head> != (Head::size() >> 1) && opened<Head> < N>>
{
    using type = typename append_character<
        array<join_arrays<Head, string<'('>>, join_arrays<Head, string<')'>>>, N
    >::type;
};


template<class Head, size_t N>
struct append_character<array<Head>, N,
                        std::enable_if_t<Head::size() < (N << 1) && opened<Head> == N>>
{
    using type = typename append_character<
        array<join_arrays<Head, string<')'>>>, N
    >::type;
};


template<size_t N>
using parentheses = typename append_character<array<>, N>::type;

int main()
{   
    each(parentheses<5>{}, [](auto value) {
        std::cout << value << '\n';
    });
    return 0;
}